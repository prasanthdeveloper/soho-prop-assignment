SohoPropAssignment::Application.routes.draw do

  root 'pages#home'
  get 'users' => 'users#index'
  post 'users/import' => 'users#import_csv_data', as: :import_csv_data
end
