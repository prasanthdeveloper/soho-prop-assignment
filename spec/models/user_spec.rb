require 'rails_helper'

RSpec.describe User, type: :model do
  valid_attrributes = {name: "Random Name", number: 23, description: "Random Description", date: Date.today.strftime("%Y-%m-%d")}
  subject {described_class.new(valid_attrributes)}

  it "is not valid without a name" do 
    subject.name = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a date" do 
    subject.date = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a number" do 
    subject.number = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a description" do
    subject.description = nil
    expect(subject).to_not be_valid
  end

  it "is valid when all required attributes are present" do 
    expect(subject).to be_valid
  end
end
