FactoryGirl.define do
  factory :user do
    name "MyString"
    date "2018-11-15"
    number 1
    description "MyText"
  end
end
