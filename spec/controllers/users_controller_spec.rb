require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe "GET index" do 
    it "assigns @users" do
      users = User.all
      get :index
      expect(assigns(:users)).to eq(users)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end
end
