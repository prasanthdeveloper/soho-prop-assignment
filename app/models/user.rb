class User < ActiveRecord::Base
  validates :name, :date, :number, :description, presence: true

  class << self
    def import_data(csv_content)
      failed_tries = []
      csv_content.each do |row|
        user = User.new(row.to_hash)
        failed_tries << row unless user.save
      end
      failed_tries.count
    end
  end
end
