class UsersController < ApplicationController
  require 'csv'
  CSV_NAME = 'soho-coding-test-file.csv'

  before_action :check_file, only: :import_csv_data
  before_action :destroy_existing_list, only: :import_csv_data

  def index
    @q = User.ransack(params[:q])
    @users = @q.result(distinct: true)
  end

  def import_csv_data
    read_file
    parse_csv
    import_data
    if @failed_count > 0
      flash[:error] = "#{view_context.pluralize(@failed_count, 'user')} dropped due to missing data"
    else
      flash[:success] = "All users imported!"
    end
    redirect_to users_path
  end

  private

  def check_file
    if params[:file] && params[:file].original_filename != CSV_NAME
      flash[:error] = "File not recognized. Pls upload Soho Test CSV file"
      redirect_to :back
    end
  end

  def read_file
    @csv_file = File.read(params[:file].path)
  end

  def parse_csv
    @csv_content = CSV.parse(@csv_file, :headers => true)
  end

  def import_data
    @failed_count = User.import_data(@csv_content)
  end

  def destroy_existing_list
    User.destroy_all
  end
end
